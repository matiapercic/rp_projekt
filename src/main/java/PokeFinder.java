
import lombok.SneakyThrows;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.*;



public class PokeFinder extends PokeOption implements ActionListener {


    JFrame frame2 = new JFrame();
    static JTextField textField1 = new JTextField();
    JLabel label1 = new JLabel("Primarni tip");

    static JTextField textField2 = new JTextField();
    JLabel label2 = new JLabel("Sekundarni tip");

    static JTextField textField3 = new JTextField();
    JLabel label3 = new JLabel("Primarna boja");

    static JTextField textField4 = new JTextField();
    JLabel label4 = new JLabel("Sekundarna boja");

    static JTextField textField5 = new JTextField();
    JLabel label5 = new JLabel("Metoda evolucija");

    static JTextField textField6 = new JTextField();
    JLabel label6 = new JLabel("Regionalna forma");

    JButton button2 = new JButton("Pogodi pokemona");
    JButton back = new JButton("Povratak na odabir");

    int dexNumber;
    String name=null;
    String primaryType;
    String secondaryType;
    String primaryColour;
    String secondaryColour;
    int stage;
    String dexEntry;
    String evolutionMethod;
    String form;
    int baseStatTotal;


    public String primarni_tip = textField1.getText();
    public String sekundarni_tip = textField2.getText();
    public String primarna_boja = textField3.getText();
    public String sekundarna_boja = textField4.getText();
    public String evolucija = textField5.getText();
    public String forma = textField6.getText();


    PokeFinder(int checkOutput) {


        this.checkOutput = checkOutput;

        switch (checkOutput){
            case 1: {

                textField6.setBounds(100, 250, 100, 20);
                textField6.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                label6.setBounds(250, 250, 400, 20);
                label6.setForeground(Color.white);
                textField6.addActionListener(this);
                label6.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                frame2.add(textField6);
                frame2.add(label6);

                break;
                     }

            case 2: {

                textField5.setBounds(100, 250, 100, 20);
                textField5.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"), 3));
                label5.setBounds(250, 250, 400, 20);
                label5.setForeground(Color.white);
                textField5.addActionListener(this);
                label5.setFont(new Font("Pokemon GB", Font.PLAIN, 15));


                frame2.add(textField5);
                frame2.add(label5);

                break;
                 }

            case 3: {

                textField5.setBounds(100, 250, 100, 20);
                textField5.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                label5.setBounds(250, 250, 400, 20);
                label5.setForeground(Color.white);
                textField5.addActionListener(this);
                label5.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                textField6.setBounds(100, 300, 100, 20);
                textField6.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                textField6.addActionListener(this);
                label6.setBounds(250, 300, 400, 20);
                label6.setForeground(Color.white);
                label6.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                frame2.add(textField5);
                frame2.add(label5);
                frame2.add(textField6);
                frame2.add(label6);

                break;
            }

            case 4:{

                textField4.setBounds(100, 250, 100, 20);
                textField4.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                label4.setBounds(250, 250, 400, 20);
                label4.setForeground(Color.white);
                textField4.addActionListener(this);
                label4.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                frame2.add(textField4);
                frame2.add(label4);

                break;
            }

            case 5: {

                textField4.setBounds(100, 250, 100, 20);
                textField4.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                label4.setBounds(250, 250, 400, 20);
                label4.setForeground(Color.white);
                textField4.addActionListener(this);
                label4.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                textField6.setBounds(100, 300, 100, 20);
                textField6.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                textField6.addActionListener(this);
                label6.setBounds(250, 300, 400, 20);
                label6.setForeground(Color.white);
                label6.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                frame2.add(textField4);
                frame2.add(label4);
                frame2.add(textField6);
                frame2.add(label6);

                break;
            }

            case 6:{

                textField4.setBounds(100, 250, 100, 20);
                textField4.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                label4.setBounds(250, 250, 500, 20);
                label4.setForeground(Color.white);
                textField4.addActionListener(this);
                label4.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                textField5.setBounds(100, 300, 100, 20);
                textField5.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                textField5.addActionListener(this);
                label5.setBounds(250, 300, 500, 20);
                label5.setForeground(Color.white);
                label5.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                frame2.add(textField5);
                frame2.add(label5);
                frame2.add(textField4);
                frame2.add(label4);

                break;
            }

            case 7: {

                textField4.setBounds(100, 250, 100, 20);
                textField4.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                label4.setBounds(250, 250, 500, 20);
                label4.setForeground(Color.white);
                textField4.addActionListener(this);
                label4.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                textField5.setBounds(100, 300, 100, 20);
                textField5.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                textField5.addActionListener(this);
                label5.setBounds(250, 300, 500, 20);
                label5.setForeground(Color.white);
                label5.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                textField6.setBounds(100, 350, 100, 20);
                textField6.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
                textField6.addActionListener(this);
                label6.setBounds(250, 350, 500, 20);
                label6.setForeground(Color.white);
                label6.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

                frame2.add(textField4);
                frame2.add(label4);
                frame2.add(textField5);
                frame2.add(label5);
                frame2.add(textField6);
                frame2.add(label6);

                break;
            }
        }

        textField1.setBounds(100, 100, 100, 20);
        textField1.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
        textField1.addActionListener(this);
        label1.setBounds(250, 100, 500, 20);
        label1.setForeground(Color.white);
        label1.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

        textField2.setBounds(100, 150, 100, 20);
        textField2.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
        textField2.addActionListener(this);
        label2.setBounds(250, 150, 500, 20);
        label2.setForeground(Color.white);
        label2.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

        textField3.setBounds(100, 200, 100, 20);
        textField3.setBorder(BorderFactory.createLineBorder(Color.decode("#003A70"),3));
        textField3.addActionListener(this);
        label3.setBounds(250, 200, 500, 20);
        label3.setForeground(Color.white);
        label3.setFont(new Font("Pokemon GB", Font.PLAIN, 15));

        button2.setBounds(400, 600, 200, 20);
        button2.setFont(new Font("Pokemon GB", Font.PLAIN, 10));
        button2.addActionListener(new ActionListener() {
            @SneakyThrows
            @Override

                public void actionPerformed(ActionEvent e) {

                PokeFinder.super.optionFrame.dispose();
                if(textField1.getText().isEmpty() || textField2.getText().isEmpty() || textField3.getText().isEmpty() ){
                    System.out.println("---  ERROR - FIRST 3 TEXTFIELDS MUST BE ENTERED ---");
                    System.out.println("----------------------------------------------------");
                    frame2.dispose();
                    PokeFinder newFinder= new PokeFinder(checkOutput);
                }
                else {
                    frame2.dispose();
                    Pokemon displaypage = new Pokemon(checkOutput);
                }
            }
        }
        );

        back.setBounds(80, 600, 250, 20);
        back.setFont(new Font("Pokemon GB", Font.PLAIN, 10));
        back.addActionListener(new ActionListener() {
            @SneakyThrows
            @Override

            public void actionPerformed(ActionEvent e) {
                frame2.dispose();
                PokeFinder.super.optionFrame.dispose();
                PokeOption optionPage = new PokeOption();
            }
        });

        frame2.add(button2);
        frame2.add(back);
        frame2.add(textField1);
        frame2.add(label1);
        frame2.add(textField2);
        frame2.add(label2);
        frame2.add(textField3);
        frame2.add(label3);
        frame2.getContentPane().setBackground(new Color(52,102,175));
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame2.setSize(700, 700);
        frame2.setLayout(null);
        frame2.setVisible(true);


    }

    public void findPokemon(int checkOutput) throws SQLException {



        this.checkOutput = checkOutput;


        String url = "jdbc:sqlserver://localhost:1433;;database=PokeGuesserProject";
        String usr = "rpuser";
        String pass = "24601";

        Connection con = DriverManager.getConnection(url, usr, pass);
        CallableStatement cs = null;


        switch (checkOutput){

            case 0: {

                cs = con.prepareCall(String.format("{call dbo.ProcedureName5(%s,%s,%s)}",
                        primarni_tip,
                        sekundarni_tip,
                        primarna_boja
                ));

                break;
            }

            case 1: {

                cs = con.prepareCall(String.format("{call dbo.ProcedureFinalForm(%s,%s,%s,%s)}",
                        primarni_tip,
                        sekundarni_tip,
                        primarna_boja,
                        forma
                ));

                break;
            }

            case 2: {

                System.out.println(evolucija);

                cs = con.prepareCall(String.format("{call dbo.ProcedureFinalEvolution(%s,%s,%s,%s)}",
                        primarni_tip,
                        sekundarni_tip,
                        primarna_boja,
                        evolucija
                ));

                break;
            }

            case 3: {

                cs = con.prepareCall(String.format("{call dbo.ProcedureFinalFormEvolution(%s,%s,%s,%s,%s)}",
                        primarni_tip,
                        sekundarni_tip,
                        primarna_boja,
                        evolucija,
                        forma
                ));

                break;
            }

            case 4: {

                cs = con.prepareCall(String.format("{call dbo.ProcedureName5(%s,%s,%s,%s)}",
                        primarni_tip,
                        sekundarni_tip,
                        primarna_boja,
                        sekundarna_boja
                ));

                break;
            }

            case 5: {

                cs = con.prepareCall(String.format("{call dbo.ProcedureFinalColourForm(%s,%s,%s,%s,%s)}",
                        primarni_tip,
                        sekundarni_tip,
                        primarna_boja,
                        sekundarna_boja,
                        forma
                ));

                break;
            }

            case 6: {

                cs = con.prepareCall(String.format("{call dbo.ProcedureFinalColourEvolution(%s,%s,%s,%s,%s)}",
                        primarni_tip,
                        sekundarni_tip,
                        primarna_boja,
                        sekundarna_boja,
                        evolucija
                ));

                break;
            }

            case 7: {

                cs = con.prepareCall(String.format("{call dbo.ProcedureName5(%s,%s,%s,%s,%s,%s)}",
                        primarni_tip,
                        sekundarni_tip,
                        primarna_boja,
                        sekundarna_boja,
                        evolucija,
                        forma
                ));

                break;
            }

            default: {

                System.out.println("-- ERROR - SOMETHING WENT WRONG -> CAN'T DISPLAY NEXT PAGE --");
                System.out.println("--------------------------------------------------------------");
            }
        }

        ResultSet rs = cs.executeQuery();

        while (rs.next()) {

            dexNumber = rs.getInt("Dex_Number");
            name = rs.getString("Name");
            primaryType = rs.getString("Primary_type");
            secondaryType = rs.getString("Secondary_type");
            primaryColour = rs.getString("Primary_colour");
            secondaryColour = rs.getString("Secondary_colour");
            stage = rs.getInt("Stage_Number");
            dexEntry = rs.getString("Dex_Entry");
            evolutionMethod = rs.getString("Evolution_method_Name");
            form = rs.getString("Form_Name");
            baseStatTotal = rs.getInt("Base_Stat_Total");

        }
    }


}

