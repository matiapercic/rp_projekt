import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class NewPage implements ActionListener {
    JFrame frame1= new JFrame();
    JButton button1=new JButton("Next");
    JLabel mainLable=new JLabel("PokéGuesserProject");

    JTextArea desc=new JTextArea("Dobro dosli u PokeGuesserProject!\n" +
            " \n"+
            " \n"+
            "Ovaj program pokusati ce pogoditi na kojega Pokémon-a mislite. Naravno, za takvo pogadjanje morate nam dati neke informacije.\n"+
            " \n"+
            "Na iducem ekranu prikazati ce se izbor podataka koji se unose, pritom cega se primarni i sekundarni tip Pokémon-a kao i njegova dominantna (primarna) boja moraju unijeti.\n"+
            " \n" +
            "Ukoliko postoji vise Pokémon-a koji imaju dane atribute, prikazati ce se onaj koji ima najveci Base Stat Total.");

    public NewPage() throws IOException {

        button1.setBounds(500,600,200,20);
        button1.setFont(new Font("Pokemon GB",Font.PLAIN,10));
        button1.addActionListener(this);


        mainLable.setFont(new Font("Pokemon Solid",Font.PLAIN,50));
        mainLable.setForeground(new Color(255,203,5));
        mainLable.setBounds(460, 50, 1000, 100);

        desc.setFont(new Font("Pokemon GB",Font.PLAIN,15));
        desc.setBounds(100,230,1000,300);
        desc.setForeground(Color.white);
        desc.setBackground(new Color(52,102,175));
        desc.setLineWrap(true);
        desc.setWrapStyleWord(true);

        frame1.add(mainLable);
        frame1.add(desc);
        frame1.add(button1);
        frame1.getContentPane().setBackground(new Color(52,102,175));
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame1.setSize(1620,780);
        frame1.setLayout(null);
        frame1.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        frame1.dispose();
        if(e.getSource()==button1){
            PokeOption optionPage= new PokeOption();

        }
    }
}
