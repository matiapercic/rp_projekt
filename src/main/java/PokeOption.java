import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.awt.Font.PLAIN;

public class PokeOption implements ActionListener {

    JFrame optionFrame=new JFrame();
    JLabel optionOpis=new JLabel("Odaberi koje podatke zelis unijeti");

    JLabel check1=new JLabel("Primarni tip");
    JLabel check2=new JLabel("Sekundarni tip");
    JLabel check3=new JLabel("Primarna boja");
    JCheckBox check4=new JCheckBox("Sekundarna boja");
    JCheckBox check5=new JCheckBox("Metoda evolucije");
    JCheckBox check6=new JCheckBox("Regionalna forma");

    JLabel checkOpis1=new JLabel("Primarni tip: Bug, Dark, Dragon, Electric, Fairy, Fighting, Fire, Flying, Ghost," +
            " Grass, Ground, Ice, Normal, Poison, Psychic, Rock, Steel, Water");
    JLabel checkOpis2=new JLabel("Sekundarni tip: Bug, Dark, Dragon, Electric, Fairy, Fighting, Fire, Flying, Ghost" +
            " Grass, Ground, Ice, None, Normal, Poison, Psychic, Rock, Steel, Water");
    JLabel checkOpis3=new JLabel("Primarna boja: Black, Blue, Brown, Gray, Green, Orange, Pink, Purple, Red, " +
            "White, Yellow");
    JLabel checkOpis4=new JLabel("Sekundarna boja: Black, Blue, Brown, Gray, Green, None, Orange, Pink, Purple, Red, " +
            "White, Yellow");
    JLabel checkOpis5=new JLabel("Metoda Evolucije: Base_form, Friendship, Holding item, Item, Level_up, Level_up_move," +
            " Level_up_time, Location, Trade, Unique");
    JLabel checkOpis6=new JLabel("Regionalna forma: Alolan, Galarian, Hisuian, Original");

    JButton next=new JButton("Next");

    int checkOutput;

    PokeOption(){
        optionOpis.setFont(new Font("Pokemon Solid", PLAIN,30));
        optionOpis.setForeground(new Color(255,203,5));
        optionOpis.setBounds(450,50,1000,100);

        check1.setBounds(300,150,500,25);
        check1.setForeground(Color.white);
        check1.setFont(new Font("Pokemon GB", PLAIN, 13));

        check2.setBounds(300,230,500,25);
        check2.setForeground(Color.white);
        check2.setFont(new Font("Pokemon GB", PLAIN, 13));

        check3.setBounds(300,310,500,25);
        check3.setForeground(Color.white);
        check3.setFont(new Font("Pokemon GB", PLAIN, 13));

        check4.setBounds(800,150,500,25);
        check4.setFont(new Font("Pokemon GB", PLAIN, 13));
        check4.setForeground(Color.white);
        check4.setBackground(new Color(52,102,175));

        check5.setBounds(800,230,500,25);
        check5.setFont(new Font("Pokemon GB", PLAIN, 13));
        check5.setForeground(Color.white);
        check5.setBackground(new Color(52,102,175));

        check6.setBounds(800,310,500,25);
        check6.setFont(new Font("Pokemon GB", PLAIN, 13));
        check6.setForeground(Color.white);
        check6.setBackground(new Color(52,102,175));


        checkOpis1.setBounds(13,370,1500,20);
        checkOpis1.setForeground(Color.white);
        checkOpis1.setFont(new Font("Calibri",PLAIN,20));

        checkOpis2.setBounds(13,410,1500,20);
        checkOpis2.setForeground(Color.white);
        checkOpis2.setFont(new Font("Calibri",PLAIN,20));

        checkOpis3.setBounds(13,450,1500,20);
        checkOpis3.setForeground(Color.white);
        checkOpis3.setFont(new Font("Calibri",PLAIN,20));

        checkOpis4.setBounds(13,490,1500,20);
        checkOpis4.setForeground(Color.white);
        checkOpis4.setFont(new Font("Calibri",PLAIN,20));

        checkOpis5.setBounds(13,530,1500,20);
        checkOpis5.setForeground(Color.white);
        checkOpis5.setFont(new Font("Calibri",PLAIN,20));

        checkOpis6.setBounds(13,570,1500,20);
        checkOpis6.setForeground(Color.white);
        checkOpis6.setFont(new Font("Calibri",PLAIN,20));

        next.setFont(new Font("Pokemon GB", PLAIN,10));
        next.setBounds(450,650,200,20);
        next.addActionListener(this);

        optionFrame.add(optionOpis);
        optionFrame.add(check1);
        optionFrame.add(check2);
        optionFrame.add(check3);
        optionFrame.add(check4);
        optionFrame.add(check5);
        optionFrame.add(check6);
        optionFrame.add(checkOpis1);
        optionFrame.add(checkOpis2);
        optionFrame.add(checkOpis3);
        optionFrame.add(checkOpis4);
        optionFrame.add(checkOpis5);
        optionFrame.add(checkOpis6);
        optionFrame.add(next);
        optionFrame.getContentPane().setBackground(new Color(52,102,175));
        optionFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        optionFrame.setSize(1620,780);
        optionFrame.setLayout(null);
        optionFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        optionFrame.dispose();


        if (e.getSource() == next) {

        if (!check4.isSelected() && !check5.isSelected() && !check6.isSelected())
            checkOutput = 0;
        else if (!check4.isSelected() && !check5.isSelected() && check6.isSelected())
            checkOutput = 1;
        else if (!check4.isSelected() && check5.isSelected() && !check6.isSelected())
            checkOutput = 2;
        else if (!check4.isSelected() && check5.isSelected() && check6.isSelected())
            checkOutput = 3;
        else if (check4.isSelected() && !check5.isSelected() && !check6.isSelected())
            checkOutput = 4;
        else if (check4.isSelected() && !check5.isSelected() && check6.isSelected())
            checkOutput = 5;
        else if (check4.isSelected() && check5.isSelected() && !check6.isSelected())
            checkOutput = 6;
        else if (check4.isSelected() && check5.isSelected() && check6.isSelected())
            checkOutput = 7;

        PokeFinder finderPage = new PokeFinder(checkOutput);

        System.out.println(checkOutput);


        }
    }
}
