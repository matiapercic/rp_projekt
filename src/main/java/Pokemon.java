import com.microsoft.sqlserver.jdbc.SQLServerError;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import lombok.SneakyThrows;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;

public class Pokemon extends PokeFinder {


    JFrame frame3 = new JFrame();
    JFrame pictureFrame= new JFrame();
    JFrame errorFrame=new JFrame("ERROR");
    JLabel label = new JLabel("Found pokemon: ");
    JLabel errorLabel=new JLabel("POKÉMON NOT FOUND - TRY AGAIN");
    JButton ok=new JButton("ok");
    JButton back=new JButton("Povratak na odabir");
    JButton picture= new JButton("Prikazi sliku");
    JButton close=new JButton("Zatvori program");

    JLabel dexNo=new JLabel("Dex Number: ");
    JLabel name=new JLabel("Ime: ");
    JLabel bst=new JLabel("Base stat total: ");
    JLabel form=new JLabel("Regionalna forma: ");
    JLabel type=new JLabel("Primarni i sekundarni tip: ");
    JLabel colour=new JLabel("Primarna i sekundarna boja: ");
    JLabel stage=new JLabel("Evolucijski stadij: ");
    JLabel evolution=new JLabel("Nacin evolucije: ");
    JLabel dex=new JLabel("Pokédex zapis: ");


    String basic="Pokemon GB";
    String special="Pokemon Unown GB";
    String text;


    JLabel labelImage=new JLabel(" ");


    Pokemon(int checkOutput) throws SQLException, IOException {
        super(checkOutput);
        frame2.dispose();


        findPokemon(checkOutput);

        if (super.name == null) {

            System.out.println("---  ERROR - POKEMON NOT FOUND -> TRY AGAIN  --- ");
            System.out.println("---------------------------------------------------");

            Pokemon.super.optionFrame.dispose();
            PokeOption optionPage=new PokeOption();

        } else {

            JLabel finalDexNumber = new JLabel(String.valueOf(super.dexNumber));
            JLabel finalName = new JLabel(super.name);
            JLabel finalPrimaryType = new JLabel(super.primaryType);
            JLabel finalSecondaryType = new JLabel(super.secondaryType);
            JLabel finalPrimaryColour = new JLabel(super.primaryColour);
            JLabel finalSecondaryColour = new JLabel(super.secondaryColour);
            JLabel finalStage = new JLabel(String.valueOf(super.stage));
            JTextArea finalDexEntry = new JTextArea(super.dexEntry);
            JLabel finalEvolution = new JLabel(super.evolutionMethod);
            JLabel finalForm = new JLabel(super.form);
            JLabel finalBaseStatTotal = new JLabel(String.valueOf(super.baseStatTotal));


            System.out.println(super.name);

            if (finalName.getText() == "Pichu" || finalName.getText() == "Garchomp")
                text = special;
            else
                text = basic;

            label.setBounds(500, 50, 400, 50);
            label.setForeground(new Color(255,203,5));
            label.setFont(new Font("Pokemon Solid", Font.PLAIN, 45));


            dexNo.setBounds(150, 150, 200, 20);
            dexNo.setForeground(Color.white);
            dexNo.setFont(new Font(text, Font.PLAIN, 15));

            finalDexNumber.setBounds(320, 150, 200, 20);
            finalDexNumber.setForeground(Color.white);
            finalDexNumber.setFont(new Font(text, Font.BOLD, 15));


            name.setBounds(700, 150, 200, 20);
            name.setForeground(Color.white);
            name.setFont(new Font(text, Font.PLAIN, 15));

            finalName.setBounds(770, 150, 400, 20);
            finalName.setForeground(Color.white);
            finalName.setFont(new Font(text, Font.BOLD, 15));


            bst.setBounds(150, 200, 400, 20);
            bst.setForeground(Color.white);
            bst.setFont(new Font(text, Font.PLAIN, 15));

            finalBaseStatTotal.setBounds(400, 200, 200, 20);
            finalBaseStatTotal.setForeground(Color.white);
            finalBaseStatTotal.setFont(new Font(text, Font.BOLD, 15));


            form.setBounds(700, 200, 400, 20);
            form.setForeground(Color.white);
            form.setFont(new Font(text, Font.PLAIN, 15));

            finalForm.setBounds(970, 200, 200, 20);
            finalForm.setForeground(Color.white);
            finalForm.setFont(new Font(text, Font.BOLD, 15));


            type.setBounds(150, 250, 500, 20);
            type.setForeground(Color.white);
            type.setFont(new Font(text, Font.PLAIN, 15));

            finalPrimaryType.setBounds(700, 250, 200, 20);
            finalPrimaryType.setForeground(Color.white);
            finalPrimaryType.setFont(new Font(text, Font.BOLD, 15));

            finalSecondaryType.setBounds(900, 250, 200, 20);
            finalSecondaryType.setForeground(Color.white);
            finalSecondaryType.setFont(new Font(text, Font.BOLD, 15));


            colour.setBounds(150, 300, 500, 20);
            colour.setForeground(Color.white);
            colour.setFont(new Font(text, Font.PLAIN, 15));

            finalPrimaryColour.setBounds(700, 300, 200, 20);
            finalPrimaryColour.setForeground(Color.white);
            finalPrimaryColour.setFont(new Font(text, Font.BOLD, 15));

            finalSecondaryColour.setBounds(900, 300, 200, 20);
            finalSecondaryColour.setForeground(Color.white);
            finalSecondaryColour.setFont(new Font(text, Font.BOLD, 15));


            evolution.setBounds(150, 350, 500, 20);
            evolution.setForeground(Color.white);
            evolution.setFont(new Font(text, Font.PLAIN, 15));

            finalEvolution.setBounds(400, 350, 200, 20);
            finalEvolution.setForeground(Color.white);
            finalEvolution.setFont(new Font(text, Font.BOLD, 15));

            stage.setBounds(700, 350, 500, 20);
            stage.setForeground(Color.white);
            stage.setFont(new Font(text, Font.PLAIN, 15));

            finalStage.setBounds(990, 350, 200, 20);
            finalStage.setForeground(Color.white);
            finalStage.setFont(new Font(text, Font.BOLD, 15));


            dex.setBounds(150, 400, 500, 20);
            dex.setForeground(Color.white);
            dex.setFont(new Font(text, Font.PLAIN, 15));

            finalDexEntry.setBounds(150, 440, 1000, 75);
            finalDexEntry.setFont(new Font(text, Font.BOLD, 15));
            finalDexEntry.setForeground(Color.white);
            finalDexEntry.setBackground(new Color(52,102,175));
            finalDexEntry.setLineWrap(true);
            finalDexEntry.setWrapStyleWord(true);



            back.setBounds(150, 600, 250, 20);
            back.setFont(new Font("Pokemon GB", Font.PLAIN, 10));
            back.addActionListener(new ActionListener() {
                @SneakyThrows
                @Override

                public void actionPerformed(ActionEvent e) {
                    frame3.dispose();
                    Pokemon.super.optionFrame.dispose();
                    PokeOption optionPage = new PokeOption();
                }
            });

            Image imgTry=new ImageIcon(this.getClass().getResource(super.form +"_"+super.name+".jpg")).getImage();
            labelImage.setIcon(new ImageIcon(imgTry));
            labelImage.setBounds(100,200,450,450);

            JLabel nameLabel=new JLabel();
            nameLabel.setFont(new Font("Pokemon GB", Font.PLAIN, 13));
            nameLabel.setForeground(Color.white);
            nameLabel.setText("Ime pokemona : " + super.name);
            nameLabel.setBounds(100,100,500,50);

            picture.setBounds(500, 600, 250, 20);
            picture.setFont(new Font("Pokemon GB", Font.PLAIN, 10));
            picture.addActionListener(new ActionListener() {
                @SneakyThrows
                @Override

                public void actionPerformed(ActionEvent e) {
                    Pokemon.super.optionFrame.dispose();

                    pictureFrame.add(nameLabel);
                    pictureFrame.add(labelImage);
                    pictureFrame.getContentPane().setBackground(new Color(52,102,175));
                    pictureFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    pictureFrame.setSize(700, 780);
                    pictureFrame.setLayout(null);
                    pictureFrame.setVisible(true);
                }
            });

            close.setBounds(850, 600, 250, 20);
            close.setFont(new Font("Pokemon GB", Font.PLAIN, 10));
            close.addActionListener(new ActionListener() {
                @SneakyThrows
                @Override

                public void actionPerformed(ActionEvent e) {
                    frame3.dispose();
                    Pokemon.super.optionFrame.dispose();
                }
            });





            frame3.add(label);
            frame3.add(finalDexNumber);
            frame3.add(finalName);
            frame3.add(finalBaseStatTotal);
            frame3.add(finalForm);
            frame3.add(finalPrimaryType);
            frame3.add(finalSecondaryType);
            frame3.add(finalPrimaryColour);
            frame3.add(finalSecondaryColour);
            frame3.add(finalStage);
            frame3.add(finalEvolution);
            frame3.add(finalDexEntry);
            frame3.add(dexNo);
            frame3.add(name);
            frame3.add(bst);
            frame3.add(form);
            frame3.add(type);
            frame3.add(colour);
            frame3.add(evolution);
            frame3.add(stage);
            frame3.add(dex);
            frame3.add(back);
            frame3.add(picture);
            frame3.add(close);
            frame3.getContentPane().setBackground(new Color(52,102,175));
            frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame3.setSize(1620, 780);
            frame3.setLayout(null);
            frame3.setVisible(true);

        }

    }
}



