import org.junit.jupiter.api.Test;

import java.sql.*;

import static org.junit.jupiter.api.Assertions.*;

class PikachuTest {


    PikachuTest() throws SQLException {
    }

    @Test
    void nullShouldReturnIllegalStatement() throws SQLException {
        Pikachu pikaTest = new Pikachu();

        assertThrows(IllegalArgumentException.class, () -> {
            pikaTest.namePikachu = "null";

        });
    }

    @Test
    void nameShouldBePikachu() throws SQLException {
        Pikachu pikaTest = new Pikachu();
        assertEquals("Pikachu", pikaTest.namePikachu);
    }

    @Test
    void typeOneShouldBeElectric() throws SQLException {
        Pikachu pikaTest = new Pikachu();
        assertEquals("Electric", pikaTest.typeOnePikachu);
    }

    @Test
    void typeTwoShouldBeElectric() throws SQLException {
        Pikachu pikaTest = new Pikachu();
        assertEquals("None", pikaTest.typeTwoPikachu);
    }

    @Test
    void colourOneShouldBeYellow() throws SQLException {
        Pikachu pikaTest = new Pikachu();
        assertEquals("Yellow", pikaTest.colourOnePikachu);
    }

    @Test
    void colourTwoShouldBeNone() throws SQLException {
        Pikachu pikaTest = new Pikachu();
        assertEquals("None", pikaTest.colourTwoPikachu);
    }

    @Test
    void evolutionMethodShouldBeBaseForm() throws SQLException {
        Pikachu pikaTest = new Pikachu();
        assertEquals("Friendship", pikaTest.evolutionMethodPikachu);
    }

    @Test
    void regionalFormShouldBeOriginal() throws SQLException {
        Pikachu pikaTest = new Pikachu();
        assertEquals("Original", pikaTest.regionalFormPikachu);
    }
}